package com.andre.hashMap;

public class HashNode<K,V> {
    private int hash;
    private K key;
    private V value;
    private HashNode<K,V> next;

    public HashNode(int hash, Object key, Object value, HashNode<K, V> next) {
        this.hash = hash;
        this.key = (K)key;
        this.value = (V)value;
        this.next = null;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public HashNode<K, V> getNext() {
        return next;
    }

    public void setNext(HashNode<K, V> next) {
        this.next = next;
    }
}
