package com.andre.hashMap;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static java.util.Objects.hash;

public class HashMap<K, V> implements Map<K, V> {
    private int n = 16;
    private HashNode<K, V>[] initArr = new HashNode[n];
    private int size = 0;

    public HashMap() {
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean containsKey(Object key) {
        return get(key) != null;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override  /** Complexidade O(n) **/
    public V get(Object key) { //TODO Falta encontrar null key
        int hash = hash(key);
        int index = Math.abs(hash % n);
        HashNode<K, V> iteratorNode = initArr[index];

        if (iteratorNode == null) {
            return null;
        } else {
            if (key == null) {
                return iteratorNode.getValue();
            } else if (iteratorNode.getKey().equals(key)) {
                return iteratorNode.getValue();
            } else {
                do {
                    if (iteratorNode.getKey().equals(key)) {
                        return iteratorNode.getValue();
                    } else {
                        iteratorNode = iteratorNode.getNext();
                    }
                } while (iteratorNode != null);
                return null;
            }
        }
    }

    @Override  /** Complexidade O(n) **/
    public V put(K key, V value) { // TODO aumentar tamanho do array quando estiver quase cheio(load factor 0.75)
        int hash = hash(key); /** "hash()" dá-nos um identificador único de um objeto **/

        /** o index é o resto da divisão do "hash" pelo tamanho do array(aceitando só positivos)**/
        int index = Math.abs(hash % n); /** aqui encontra-mos o index onde o nosso objeto vai ser colocado **/
        V prevValue;
        HashNode<K, V> newNode = new HashNode<>(hash, key, value, null);
        HashNode<K, V> iteratorNode = initArr[index];

        if (iteratorNode == null) { /** se o index apontar para um espaço vazio **/
            initArr[index] = newNode;
            size++;
            return null;
        } else { /** se o index ja tiver objectos inseridos **/
            do {
                if (hash == hash(iteratorNode.getKey())) {
                    /** se os objetos tiverem o mesmo hash mantem-se o que já estava inserido**/
                    prevValue = iteratorNode.getValue();
                    return prevValue;
                } else {

                    if (iteratorNode.getNext() != null) { /** usamos esta condição para nao obter "NullPointerException" **/
                        iteratorNode = iteratorNode.getNext(); /** avança-mos um node **/
                    }
                }
            } while (iteratorNode.getNext() != null);
            iteratorNode.setNext(newNode);
            size++;
        }
        return null;
    }

    @Override /** Complexidade O(n) **/
    public V remove(Object key) { // TODO Falta remover null se nao se encontrar na primeira casa, entre outros
        int hash = hash(key);
        int index = Math.abs(hash % n);
        HashNode<K, V> prevNode = initArr[index];
        HashNode<K, V> iteratorNode = initArr[index];
        V removed;

        if (iteratorNode == null) { /** se o index apontar para um espaço vazio **/
            return null;
        /** se a key for null ou a key for igual à key do objeto alojado naquele index **/
        } else if (key == null || iteratorNode.getKey().equals(key)) {
            removed = iteratorNode.getValue();
            /** o seguinte node fica guardado na primeira posição do index, o anterior é removido **/
            initArr[index] = iteratorNode.getNext();
            size--;
            return removed;
        } else { /** se a nossa key nao se encontra na primeira posição **/

            do { /** este ciclo vai percorrer a LinkedList guardada no index **/

                if (key.equals(iteratorNode.getKey())) {
                    removed = iteratorNode.getValue();
                    prevNode.setNext(iteratorNode.getNext());
                    size--;
                    return removed;
                } else {
                    prevNode = iteratorNode;
                    iteratorNode = iteratorNode.getNext();
                }
            } while (iteratorNode != null);
            return null;
        }
    }

    @Override
    public void putAll(Map m) {

    }

    @Override
    public void clear() {
        initArr = new HashNode[n];
    }

    @Override
    public Set keySet() {
        return null;
    }

    @Override
    public Collection values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }
}
