package com.andre.arrayList;


import java.util.*;

public class ArrayList<Type> implements List<Type> {

    private static final int initialCapacity = 10; //Tamanho inicial do array base para a criação do ArrayList
    private Type elementsArr[];
    private int size;


    public ArrayList() {
        // Inicia-mos um array com "x" elementos(initialCapacity) com o valor null
        this.elementsArr = (Type[]) new Object[initialCapacity];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) {
            return true;
        }
        return false;
    }

    @Override /** Complexidade O(n) **/
    public boolean contains(Object o) {
        int i = 0;
        while (elementsArr[i] != o) {
            i++;
            if (i == size) {
                return false;
            }
        }
        return true;
    }

    @Override /** Complexidade O(n) **/
    public boolean add(Type addedObject) {
        int i = 0;
        if (size == 0) {
            elementsArr[0] = addedObject;
            this.size++;
            return true;
        }

        if (size == elementsArr.length) { // não tenho a certeza se "Arrays.copyOf" aumenta a complexidade
            elementsArr = Arrays.copyOf(elementsArr, (elementsArr.length * 2));
        }

        while (elementsArr[i] != null) {
            i++;
        }
        elementsArr[i] = addedObject;
        size++;

        return true;
    }

    @Override /** Complexidade O(n) **/
    public void add(int index, Type element) {
        if ((index < 0 || index >= size())) {
            throw new IndexOutOfBoundsException(); // se o index introduzido for invalido ou a lista estiver vazia recebemos um erro:"IndexOutOfBounds"
        }

        int i = size - 1;
        while (i >= index) {
            elementsArr[i + 1] = elementsArr[i];
            i--;
        }
        elementsArr[i + 1] = element;
        size++;


    }

    @Override /** Complexidade O(n) **/
    public Type remove(int index) {
        if ((index < 0 || index >= size())) {
            throw new IndexOutOfBoundsException();
        }
        Type temp = elementsArr[index];
        int i = index;
        while (i != size - 1) {
            elementsArr[i] = elementsArr[i + 1];
            i++;
        }
        size--;
        return temp; // retorna o elemento removido
    }

    @Override /** Complexidade O(n) **/
    public boolean remove(Object objectToRemove) {
        int i = 0;
        while (elementsArr[i] != objectToRemove) {
            i++;
            if (i == size) {
                return false;
            }
        }

        while (i != size - 1) {
            elementsArr[i] = elementsArr[i + 1];
            i++;
        }
        size--;
        return true;
    }

    @Override
    public int indexOf(Object element) {
        int i = 0;
        while (i != size) {
            if (elementsArr[i] == element) {
                return i;
            }
            i++;
        }

        return -1;
    }

    @Override /** Complexidade O(n) **/
    public int lastIndexOf(Object element) {
        int i = size;
        while (i != 0) {
            if (elementsArr[i] == element) {
                return i;
            }
            i--;
        }
        return -1;
    }

    public void clear() {
        this.elementsArr = (Type[]) new Object[initialCapacity];
    }

    /** Complexidade O(1) **/
    public Type get(int index) {
        if ((index < 0 || index >= size())) {
            throw new IndexOutOfBoundsException();
        }
        return elementsArr[index];
    }

    /** Complexidade O(1) **/
    public Type set(int index, Type element) {
        if ((index < 0 || index >= size())) {
            throw new IndexOutOfBoundsException();
        }

        Type temp = elementsArr[index];
        elementsArr[index] = element;
        return temp;
    }

    public void printList() {
        int i = 0;
        while (i != size) {
            System.out.println(elementsArr[i]);
            i++;
        }
    }

    public Type[] toArray() {
        Type[] toArray = (Type[]) new Object[size];
        int i = 0;
        while (i != size) {
            toArray[i] = elementsArr[i];
            i++;
        }

        return toArray;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        int i = 0;
        if (a.length < size) {
            a = Arrays.copyOf(a, size - 1);
        }
        while (i != size) {
            a[i] = (T) elementsArr[i];
            i++;
        }

        return a;
    }

    public boolean removeAll(Collection<?> c) {
        return false;
    }

    public Iterator iterator() {
        Iterator<Type> it = new Iterator<Type>() {
            int current = 0;

            @Override
            public boolean hasNext() {
                return current != size;
            }

            @Override
            public Type next() {
                Type temp = elementsArr[current];
                current++;
                return temp;

            }
        };
        return it;
    }

    public boolean retainAll(Collection<?> c) {
        return false;
    }

    public List<Type> subList(int fromIndex, int toIndex) {
        return null;
    }

    public ListIterator<Type> listIterator() {
        return null;
    }

    public ListIterator<Type> listIterator(int index) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object value : c) {
            if (!contains(value)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Type> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Type> c) {
        return false;
    }
}
