package com.andre.treeSet;

import java.util.*;

public class TreeSet<T> implements Set<T> { //TODO /** Acabar remove **/
    private Comparator comparator = null;
    Node<T> root;
    int size = 0;

    public TreeSet() {  // iniciar tree vazia
        this.root = null;
    }

    public TreeSet(Node<T> root) { // iniciar tree com um valor
        this();
        this.root = root;
    }

    public TreeSet(Comparator comparator) {
        this();
        this.comparator = comparator;
    }

    public void insertRoot(Node<T> node) {
        this.root = node;
    }

    public Node<T> getRoot() {
        return root;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        if (this.size == 0) {
            return true;
        }
        return false;
    }

    @Override /** Complexidade O(log n) **/
    public boolean contains(Object o) {

        Node<T> iteratorNode = root;

        if (isEmpty()) {
            return false;
        } else {
            while (iteratorNode != null) {
                int comp = comparator.compare(iteratorNode.getValue(), o);
                if (comp == 0) {
                    return true;
                } else if (comp > 0) {
                    iteratorNode = iteratorNode.getLeft();
                } else if (comp < 0) {
                    iteratorNode = iteratorNode.getRight();
                }
            }
        }
        return false;
    }

    Node<T>[] nodeTracker(Object o) { //Usamos este metodo para encontrar um Objeto e devolvemos o seu node e o previousNode

        Node<T> iteratorNode = root;
        Node<T> prevNode = root;

        if (isEmpty()) {
            return null;
        } else {
            while (iteratorNode != null) {
                int comp = comparator.compare(iteratorNode.getValue(), o);
                if (comp == 0) {
                    return new Node[]{prevNode, iteratorNode};
                } else if (comp > 0) {
                    prevNode = iteratorNode;
                    iteratorNode = iteratorNode.getLeft();
                } else if (comp < 0) {
                    prevNode = iteratorNode;
                    iteratorNode = iteratorNode.getRight();
                }
            }
        }
        return null;
    }

    @Override /** Complexidade O(log n) --- Supostamente se estivesse acabado **/
    public boolean remove(Object o) {

        if (!contains(o)) {
            return false;
        } else {
            Node<T>[] nodeArr = nodeTracker(o);
            Node<T> toRemoveNode = nodeArr[1];
            Node<T> toRemoveParent = nodeArr[0];
            Node<T> iteratorNode = toRemoveNode;
            Node<T> prevNode = iteratorNode;

            // se "comp" > 0 o node a remover é o leftChild senão é o rightChild
            int comp = comparator.compare(toRemoveParent.getValue(), o);

            if (toRemoveParent == null) {
                return false;
            } else if (toRemoveNode.getRight() == null && toRemoveNode.getLeft() == null) {
                toRemoveNode = null;
                return true;

            } else if (toRemoveNode.getLeft() != null && toRemoveNode.getRight() == null) { // se o node a remover apenas tiver leftChild

                if (comp > 0) {  // se o node a remover é o leftChild do parent
                    toRemoveParent.setLeft(toRemoveNode.getLeft());
                    return true;
                } else {  // se o node a remover é o rightChild do parent
                    toRemoveParent.setRight(toRemoveNode.getLeft());
                    return true;
                }

            } else if (toRemoveNode.getRight() != null && toRemoveNode.getLeft() == null) { // se o node a remover apenas tiver rightChild

                if (comp > 0) {  // se o node a remover é o leftChild do parent
                    toRemoveParent.setLeft(toRemoveNode.getRight());
                    return true;
                } else {  // se o node a remover é o rightChild do parent
                    toRemoveParent.setRight(toRemoveNode.getRight());
                    return true;
                }
            } else if (toRemoveNode.getLeft() != null && toRemoveNode.getRight() != null) { // se o node a remover tiver dois filhos
                /** TO BE CONTINUE... **/
            }
        }
        return false;
    }


    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override /** Complexidade O(log n) **/
    public boolean add(T value) {

        int count = size;
        //Comparable<? super T>  cmp = (Comparable<? super T> ) value; Aprender a usar comparable!!
        Node<T> nodeToBeInserted = new Node(value);
        Node<T> iteratorNode = root;


        if (iteratorNode == null) {
            root = nodeToBeInserted;
            size++;
            return true;
        }

        if (comparator != null) {
            while (size == count) {

                int comp = comparator.compare(iteratorNode.getValue(), nodeToBeInserted.getValue());

                if (comp == 0) {
                    return false;
                } else if (comp < 0) {
                    if (iteratorNode.getRight() != null) {
                        iteratorNode = iteratorNode.getRight();
                    } else {
                        iteratorNode.setRight(nodeToBeInserted);
                        size++;
                        return true;
                    }
                } else if (comp > 0) {
                    if (iteratorNode.getLeft() != null) {
                        iteratorNode = iteratorNode.getLeft();
                    } else {
                        iteratorNode.setLeft(nodeToBeInserted);
                        size++;
                        return true;
                    }
                }
            } // fim do while
        } else {
            return false;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        this.root = null;
    }

    public void setComparator(Comparator<T> comparator) {
        this.comparator = comparator;
    }


    public void print() {
        printRec(root);
    }

    private void printRec(Node<T> node) { // Usamos isto para dar print das ligações do treeSet
        System.out.print("CURRENT: " + node.getValue() + " LEFT: ");
        if (node.getLeft() != null) {
            System.out.print(node.getLeft().getValue() + " RIGHT: ");
        } else {
            System.out.print("null RIGHT: ");
        }
        if (node.getRight() != null) {
            System.out.println(node.getRight().getValue());
        } else {
            System.out.println("null");
        }
        if (node.getLeft() != null) {
            printRec(node.getLeft());
        }
        if (node.getRight() != null) {
            printRec(node.getRight());
        }
    }
}