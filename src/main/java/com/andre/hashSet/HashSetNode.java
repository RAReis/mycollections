package com.andre.hashSet;

public class HashSetNode<K> {
    private int hash;
    private K key;
    private HashSetNode<K> next;

    public HashSetNode(int hash, Object key, HashSetNode<K> next) {
        this.hash = hash;
        this.key = (K)key;
        this.next = null;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public HashSetNode<K> getNext() {
        return next;
    }

    public void setNext(HashSetNode<K> next) {
        this.next = next;
    }
}
