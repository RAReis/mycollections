package com.andre.hashSet;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import static java.util.Objects.hash;

public class HashSet<T> implements Set<T> {
    private int size = 0;
    private int n = 16;
    private HashSetNode<T>[] initArr;

    public HashSet(){
        initArr = new HashSetNode[n];
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        if(size == 0){
            return true;
        }
        return false;
    }

    @Override /** Complexidade O(n) **/
    public boolean contains(Object o) { //TODO Reconhecer null
        int hash = hash(o);
        int index = Math.abs(hash % n);

        HashSetNode<T> iteratorNode = initArr[index];

        if (iteratorNode == null) {
            return false;

        } else { /** se o index ja tiver objectos inseridos **/
            do {
                if (hash == hash(iteratorNode.getKey())) {
                    return true;
                } else {

                    if (iteratorNode.getNext() != null) { /** usamos esta condição para nao obter "NullPointerException" **/
                        iteratorNode = iteratorNode.getNext(); /** avança-mos um node **/
                    }
                }
            } while (iteratorNode.getNext() != null);
            return false;
        }
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override /** Complexidade O(n) **/
    public boolean add(Object o) { //TODO resolver inserção de null
        int hash = hash(o);
        int index = Math.abs(hash % n);

        HashSetNode<T> newNode = new HashSetNode<>(hash, o, null);
        HashSetNode<T> iteratorNode = initArr[index];

        if (iteratorNode == null) { /** se o index apontar para um espaço vazio **/
            initArr[index] = newNode;
            size++;
            return true;
        } else { /** se o index ja tiver objectos inseridos **/
            do {
                if (hash == hash(iteratorNode.getKey())) {
                    /** se os objetos tiverem o mesmo hash não ha alterações**/
                    return true;
                } else {

                    if (iteratorNode.getNext() != null) { /** usamos esta condição para nao obter "NullPointerException" **/
                        iteratorNode = iteratorNode.getNext(); /** avança-mos um node **/
                    }
                }
            } while (iteratorNode.getNext() != null);
            iteratorNode.setNext(newNode);
            size++;
            return true;
        }
    }

    @Override  /** Complexidade O(n) **/
    public boolean remove(Object o) {
        int hash = hash(o);
        int index = Math.abs(hash % n);
        HashSetNode<T> prevNode = initArr[index];
        HashSetNode<T> iteratorNode = initArr[index];

        if (iteratorNode == null) { /** se o index apontar para um espaço vazio **/
            return false;
            /** se a key for null ou a key for igual à key do objeto alojado naquele index **/
        } else if (o == null || iteratorNode.getKey().equals(o)) {
            /** o seguinte node fica guardado na primeira posição do index, o anterior é removido **/
            initArr[index] = iteratorNode.getNext();
            size--;
            return true;
        } else { /** se a nossa key nao se encontra na primeira posição **/

            do { /** este ciclo vai percorrer a LinkedList guardada no index **/

                if (o.equals(iteratorNode.getKey())) {
                    prevNode.setNext(iteratorNode.getNext());
                    size--;
                    return true;
                } else {
                    prevNode = iteratorNode;
                    iteratorNode = iteratorNode.getNext();
                }
            } while (iteratorNode != null);
            return false;
        }
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {
        initArr = new HashSetNode[n];
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }
}
