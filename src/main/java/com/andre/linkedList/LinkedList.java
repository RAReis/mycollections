package com.andre.linkedList;

import java.util.*;

public class LinkedList<T> implements List<T>, Deque<T> {
    private Node<T> first;
    private Node<T> last;
    private int size = 0;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) {
            return true;
        }
        return false;
    }

    @Override /** Complexidade O(n) **/
    public boolean contains(Object node) {
        int i = 0;
        Node nodeIterator = first;
        while (i < size) {
            if (node == nodeIterator.value) {
                return true;
            }
            nodeIterator = nodeIterator.nextNode;
            i++;
        }
        return false;
    }

    @Override
    public void clear() {
        this.size = 0;
        this.first = null;
        this.last = null;
    }

    @Override /** Complexidade O(1) **/
    public boolean add(T element) {
        addLast(element);
        return true;
    }

    @Override /** Complexidade O(1) **/
    public void addFirst(T element) {
        if (first == null) {
            first = new Node(element);
            last = first;
            size++;
            return;
        }

        Node newNode = new Node(element);
        newNode.nextNode = first;
        first = newNode;
        size++;
    }

    @Override /** Complexidade O(1) **/
    public void addLast(T element) {
        if (last == null) {
            first = new Node(element);
            last = first;
            size++;
            return;
        }
        Node newNode = new Node(element);

        last.nextNode = newNode;
        last = newNode;

        size++;
    }

    @Override /** Complexidade O(n) **/
    public void add(int index, T element) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }

        if (index == 0) {
            addFirst(element);
            return;
        }

        if (index == size) {
            addLast(element);
            return;
        }

        Node newNode = new Node(element);
        Node iteratorNode = first;

        for (int i = 1; i <= index - 1; i++) {
            iteratorNode = iteratorNode.nextNode;
        }
        newNode.nextNode = iteratorNode.nextNode;
        iteratorNode.nextNode = newNode;
        size++;

    }

    public void print() {

        if (first == null) {
            System.out.println("LinkedList is empty.");
            return;
        }

        Node newNode = first;

        do {
            System.out.println(newNode.value + " ");
            newNode = newNode.nextNode;
        } while (newNode != null);
        System.out.println();
    }

    @Override /** Complexidade O(1) **/
    public T removeFirst() {
        if (size == 0) {
            throw new NoSuchElementException();
        }

        Node<T> temp = first;
        remove(0);
        return temp.value;
    }

    @Override /** Complexidade O(1) **/
    public T removeLast() {
        if (size == 0) {
            throw new NoSuchElementException();
        }

        Node<T> temp = last;
        remove(size - 1);
        return temp.value;
    }

    @Override /** Complexidade O(n) **/
    public T remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        if (size == 0) {
            return null;
        }

        Node<T> iteratorNode = first;

        for (int i = 0; i < index - 1; i++) {
            iteratorNode = iteratorNode.nextNode;
        }
        Node<T> temp = iteratorNode;

        if (index == 0) {
            first = iteratorNode.nextNode;
            size--;
        } else {
            iteratorNode.nextNode = iteratorNode.nextNode.nextNode;
            size--;
        }
        return temp.value;
    }

    @Override /** Complexidade O(n) **/
    public boolean remove(Object node) {
        int i = 0;
        Node<T> nodeIterator = first;
        while (i < size) {
            if (node == nodeIterator.value) {
                remove(i);
                return true;
            }
            nodeIterator = nodeIterator.nextNode;
            i++;
        }
        return false;
    }

    @Override /** Complexidade O(n) **/
    public boolean removeFirstOccurrence(Object o) {
        if(remove(o)){
            return true;
        }
        return false;
    }

    @Override /** Complexidade O(n) **/
    public boolean removeLastOccurrence(Object o) {
     if(contains(o)){
        int i = lastIndexOf(o);
        remove(i);
        return true;
     }
     return false;
    }

    @Override /** Complexidade O(n) **/
    public int indexOf(Object node) {

        int i = 0;
        Node<T> iteratorNode = first;
        if (node == null) {
            while (i < size) {
                if (iteratorNode.value == null) {
                    return i;
                }
                iteratorNode = iteratorNode.nextNode;
                i++;
            }
        }

        while (i < size) {
            if (iteratorNode.value == node) {
                return i;
            }
            iteratorNode = iteratorNode.nextNode;
            i++;
        }
        return -1;
    }

    @Override /** Complexidade O(n) **/
    public int lastIndexOf(Object o) {
        int i = 0;
        int j = 0;
        Node<T> iteratorNode = first;
        if (o == null) {
            while (i < size) {
                if (iteratorNode.value == null) {
                    j = i + 1;
                }
                iteratorNode = iteratorNode.nextNode;
                i++;
            }
            return j - 1;
        } else if (o != null) {

            while (i < size) {
                if (iteratorNode.value == o) {
                    j = i + 1;
                }
                iteratorNode = iteratorNode.nextNode;
                i++;
            }
            return j - 1;
        }
        return -1;

    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public T pollFirst() {
        if(size == 0) {
            return null;
        }
        return removeFirst();
    }

    @Override
    public T pollLast() {
        if(size == 0){
            return null;
        }
        return removeLast();
    }

    @Override
    public T getFirst() {
        if (size == 0) {
            return null;
        }
        return first.value;
    }

    @Override
    public T getLast() {
        if (size == 0) {
            return null;
        }
        return last.value;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(index);
        }

        if (index == 0) {
            return first.value;
        }

        Node<T> nodeToSearch = first;

        for (int i = 1; i <= index; i++) {
            nodeToSearch = nodeToSearch.nextNode;
        }
        return nodeToSearch.value;
    }

    @Override
    public T set(int index, T element) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(index);
        }

        if (index == 0) {
            Node<T> temp = first;
            first.value = element;
            return temp.value;
        }

        if (index == size) {
            Node<T> temp = last;
            last.value = element;
            return temp.value;
        }

        Node iteratorNode = first;

        for (int i = 0; i < index; i++) {
            iteratorNode = iteratorNode.nextNode;
        }
        Node<T> temp = iteratorNode;
        iteratorNode.value = element;
        return temp.value;

    }

    @Override
    public T peekFirst() {
        if(size == 0){
            return null;
        }
        return getFirst();
    }

    @Override
    public T peekLast() {
        if(size == 0){
            return null;
        }
        return getLast();
    }

    @Override
    public boolean offer(T t) {
        return false;
    }

    @Override
    public T remove() {
        return removeFirst();
    }

    @Override
    public T poll() {
        return null;
    }

    @Override
    public T element() {
        return null;
    }

    @Override
    public T peek() {
        return null;
    }

    @Override
    public void push(T t) {

    }

    @Override
    public T pop() {
        return null;
    }

    @Override
    public Iterator<T> descendingIterator() {
        return null;
    }

    @Override
    public Iterator<T> iterator() {
        Iterator<T> it = new Iterator<T>() {
            private Node<T> current = first;

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public T next() {
                T temp = current.value;
                current = current.nextNode;
                return temp;

            }
        };
        return it;
    }

    @Override
    public boolean offerFirst(T t) {
        return false;
    }

    @Override
    public boolean offerLast(T t) {
        return false;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    public class Node<T> {
        private T value;
        private Node<T> nextNode = null;

        public Node(T value) {
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?> node = (Node<?>) o;
            return Objects.equals(value, node.value) &&
                    Objects.equals(nextNode, node.nextNode);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value, nextNode);
        }
    }
}
