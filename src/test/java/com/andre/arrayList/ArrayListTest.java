package com.andre.arrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import java.util.Arrays;
import java.util.Iterator;

import static org.junit.Assert.*;

public class ArrayListTest {

    ArrayList<String> list;

    @Before
    public void setUp() {
        list = new ArrayList<String>();
    }

    @Test
    public void size() {
        assert (list.size() == 0);
        list.add("1");
        list.add("2");
        assert (list.size() == 2);

    }

    @Test
    public void isEmpty() {
        assert(list.isEmpty());
        list.add("1");
        assert(!list.isEmpty());
    }

    @Test
    public void contains() {
        list.add("1");
        list.add("2");
        assert(list.contains("1"));
        assert(list.contains("2"));
        assert(!list.contains("3"));

    }

    @Test
    public void toArray() {
    }

    @Test
    public void addUsingObject() {
        assert(list.add("1"));
        assert(list.add("2"));
        for(int i = 0; i < list.size(); i++){
            System.out.println(list.get(i));
        }
    }

    @Test
    public void addUsingIndex() {
        list.add("1");
        list.add("2");
        list.add(1,"3");
        assert(list.get(1) == "3");
        for(int i = 0; i < list.size(); i++){
            System.out.println(list.get(i));
        }
    }

    @Test
    public void toArrayReceiveingArray() {

        String[] arr = {"1", "2", "3", "4"};
        ArrayList<String> collection = new ArrayList<>();
        collection.add("1st");
        collection.add("2nd");
        collection.add("3rd");
        collection.toArray(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

        System.out.println("================");
        collection.printList();
    }

    @Test
    public void containsAllTest() {
        java.util.ArrayList<String> list1 = new java.util.ArrayList<>();
        list1.add("1st");
        list1.add("2nd");
        list1.add("3rd");

        java.util.ArrayList<String> list2 = new java.util.ArrayList<>();
        list2.add("1st");
        list2.add("qwert");
        list2.add("3rd");

        java.util.ArrayList<String> collection = new java.util.ArrayList<>();
        collection.add("1st");
        collection.add("2nd");
        collection.add("3rd");
        collection.add("4th");

        assertTrue(collection.containsAll(list1));
        assertFalse((collection.containsAll(list2)));
    }

    @Test
    public void removeUsingIndex() {
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        assert(list.remove(0) == "1st");
        assert(list.get(0) == "2nd");
    }

    @Test
    public void removeUsingObject() {
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        assertTrue(list.remove("1st"));
        assert(list.get(0) == "2nd");
    }

    @Test
    public void clear() {
        java.util.ArrayList<String> list = new java.util.ArrayList<>();
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        System.out.println();
        assert(list.size() == 3);
        list.clear();
        assert(list.size() == 0);
    }

    @Test
    public void get() {
        java.util.ArrayList<String> list = new java.util.ArrayList<>();
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        assert(list.get(0).equals("1st"));
    }

    @Test
    public void set() {
        java.util.ArrayList<String> list = new java.util.ArrayList<>();
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        assert (list.set(0, "new").equals("1st"));
        assert (list.get(0).equals("new"));
    }

    @Test
    public void indexOf() {
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        assert(list.indexOf("1st") == 0);
        assert(list.indexOf("4th") == -1);
    }

    @Test
    public void lastIndexOf() {
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        list.add("1st");
        assert(list.lastIndexOf("1st") == 3);
        assert(list.lastIndexOf("4th") == -1);
    }

    @Test
    public void listIterator() {
    }

    @Test
    public void listIterator1() {
    }

    @Test
    public void iterator() {
        list.add("1st");
        list.add("2nd");
        list.add("3rd");

        for (String s : list) {
            System.out.println(s);
        }
    }
}