package com.andre.treeSet;

import org.junit.Before;
import org.junit.Test;


import java.util.Comparator;

import static org.junit.Assert.*;


public class TreeSetTest {
    TreeSet<Integer> treeSet;

    @Before
    public void setUp() throws Exception {
        treeSet = new TreeSet<>(new IntComparator());
    }

    @Test
    public void add() {
        assertTrue(treeSet.add(10));
        assertTrue(treeSet.add(20));
        assertFalse(treeSet.add(10));
    }

    @Test
    public void contains() {
        treeSet.add(10);
        treeSet.add(20);
        treeSet.add(15);
        assert(treeSet.size == 3);
        assertFalse(treeSet.contains(3));
        assertTrue(treeSet.contains(10));
    }

    @Test
    public void removeNonExistentObject(){
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);

        assertFalse(treeSet.remove(4));
    }

    @Test
    public void removeWithNullRoot(){

        assertFalse(treeSet.remove(1));
    }

    @Test
    public void remove(){
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);

        assertFalse(treeSet.remove(4));
    }

}

class IntComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        return o1 - o2;
    }
}

