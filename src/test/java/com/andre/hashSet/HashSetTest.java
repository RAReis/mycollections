package com.andre.hashSet;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HashSetTest {
    HashSet<String> hSet;
    @Before
    public void setUp() {
         hSet = new HashSet<>();
    }

    @Test
    public void add() {
        hSet.add("First");
        hSet.add(null);
        hSet.add("Second");
        hSet.add("First");
        System.out.println(hSet.size());
    }

    @Test
    public void contains() {
        hSet.add("First");
        hSet.add(null);
        hSet.add("Second");
        hSet.add("First");
        System.out.println(hSet.size());
        System.out.println(hSet.contains("First"));
    }

    @Test
    public void remove() {
        hSet.add("First");
        hSet.add(null);
        hSet.add("Second");
        hSet.add("Third");
        hSet.add("ToRemove");
        hSet.remove("ToRemove");
        System.out.println(hSet.size());
        System.out.println(hSet.contains("ToRemove"));

    }
}