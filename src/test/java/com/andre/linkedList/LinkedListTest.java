package com.andre.linkedList;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**assert();
 assertArrayEquals();
 assertEquals();
 assertTrue();
 assertFalse();**/

public class LinkedListTest {

    LinkedList<String> list;
    @Before
    public void setUp() throws Exception {
        list = new LinkedList<>();
    }

    @Test
    public void size() {
        assertEquals(0,list.size());
        list.add("1st");
        assertEquals(1,list.size());
        list.add("2nd");
        assertEquals(2,list.size());
    }

    @Test
    public void isEmpty() {
        assertTrue(list.isEmpty());
        list.add("1st");
        assertFalse(list.isEmpty());
    }

    @Test
    public void contains() {

        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        list.add(null);
        assertFalse(list.contains("4th"));
        assertTrue(list.contains("2nd"));
        assertTrue(list.contains(null));
        //assertFalse(list.contains(null));
        System.out.println(list.contains(null));
    }

    @Test
    public void add() {
        list.add("1st");
        assertSame("1st", list.getFirst());
        assertSame("1st", list.getLast());
        list.add("2nd");
        assertSame("1st", list.getFirst());
        assertSame("2nd", list.getLast());
    }

    @Test
    public void removeUsingObject() {
        list.add("1");
        list.add("2");
        list.add("3");
        list.add(null);
        System.out.println(list.remove("2"));
        System.out.println(list.remove(null));
        list.print();
    }

    @Test
    public void removeFirst() {
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        assertEquals(3, list.size());
        list.removeFirst();
        list.print();
        assertEquals(2, list.size());
    }

    @Test
    public void removeLast() {
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        assertEquals(3, list.size());
        list.removeLast();
        list.print();
        assertEquals(2, list.size());
    }

    @Test
    public void containsAll() {
    }

    @Test
    public void addAll() {
    }

    @Test
    public void addAll1() {
    }

    @Test
    public void removeAll() {
    }

    @Test
    public void retainAll() {
    }

    @Test
    public void clear() {
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        list.print();
        list.clear();
        list.print();
    }

    @Test
    public void getNull() {
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        list.add(null);
        System.out.println(list.indexOf(null));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setIndexOutOfBounds() {
        list.set(0,"noSize");
    }

    @Test
    public void set() {
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        list.set(0,"1Zero");
        list.set(2,"3Zero");
        list.set(1,"2Zero");
        list.print();

    }

    @Test
    public void add1() {
    }

    @Test
    public void indexOf() {
        list.add("1");
        list.add("2");
        list.add("3");
        list.add(null);
        System.out.println(list.indexOf(null));
        assertEquals(1, list.indexOf("2"));
    }

    @Test
    public void lastIndexOf() {
        list.add("1");
        list.add("2");
        list.add("2");
        list.add(null);
        list.add("2");
        System.out.println(list.lastIndexOf("d"));

    }

    @Test
    public void subList() {
    }

    @Test
    public void print() {
    }

    @Test
    public void addFirst() {
        list.addFirst("1st");
        assertSame("1st", list.getFirst());
        list.addFirst("2nd");
        assertSame("2nd", list.getFirst());
        assertSame("1st", list.getLast());
    }

    @Test
    public void addLast() {
        list.addLast("last");
        assertSame("last", list.getFirst());
        assertSame("last", list.getLast());
        list.addLast("newLast");
        assertSame("newLast", list.getLast());
        assertSame("last", list.getFirst());
    }

    @Test
    public void pollFirst() {
    }

    @Test
    public void pollLast() {
    }

    @Test
    public void getFirst() {
        assertSame(null, list.getFirst());
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        assertSame("1st", list.getFirst());
    }

    @Test
    public void getLast() {
        assertSame(null, list.getLast());
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        assertSame("3rd", list.getLast());
    }

    @Test
    public void peekFirst() {
    }

    @Test
    public void peekLast() {
    }

    @Test
    public void removeFirstOccurrence() {
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        list.add("2nd");
        System.out.println(list.removeFirstOccurrence("2nd"));
        list.print();
    }

    @Test
    public void removeLastOccurrence() {
        list.add("1st");
        list.add("2nd");
        list.add("3rd");
        list.add("2nd");
        System.out.println(list.removeLastOccurrence("2nd"));
        list.print();
    }

    @Test
    public void iterator() {
        list.add("1st");
        list.add("2nd");
        list.add("3rd");


        for (String s : list) {
            System.out.println(s);
        }
    }

    @Test
    public void offer() {
    }

    @Test
    public void remove2() {
    }

    @Test
    public void poll() {
    }

    @Test
    public void element() {
    }

    @Test
    public void peek() {
    }

    @Test
    public void push() {
    }

    @Test
    public void pop() {
    }

    @Test
    public void descendingIterator() {
    }

    @Test
    public void offerFirst() {
    }

    @Test
    public void offerLast() {
    }

    @Test
    public void toArray() {
    }

    @Test
    public void toArray1() {
    }

    @Test
    public void listIterator() {
    }

    @Test
    public void listIterator1() {
    }
}