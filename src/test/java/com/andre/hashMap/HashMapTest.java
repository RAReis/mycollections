package com.andre.hashMap;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HashMapTest {
    HashMap<String , Integer> map;

    @Before
    public void setup(){
        map = new HashMap<>();
    }
    @Test
    public void size() {
    }

    @Test
    public void isEmpty() {
    }

    @Test
    public void containsKey() {
        map.put("ola", 0);
        map.put("asdsdfsd", 1);
        map.put("3", 2);
        map.put("4", 3);
        map.put("2", 7);
        map.put("2", 8);
        //map.put(null, 9);
        System.out.println(map.containsKey("ola"));
        System.out.println(map.containsKey(null));
        System.out.println(map.containsKey("4"));
    }

    @Test
    public void containsValue() {
        map.put("ola", 0);
        map.put("asdsdfsd", 1);
        map.put("3", 2);
        map.put("4", 3);
        map.put("2", 97);
        map.put("2", null);
        System.out.println(map.containsValue(null));
        System.out.println(map.containsValue(2));
        System.out.println(map.containsValue(97));
    }

    @Test
    public void get() {
        map.put("ola", 0);
        map.put("asdsdfsd", 1);
        map.put("3", 2);
        map.put("4", 3);
        map.put("2", 7);
        map.put("2", 8);
        map.put(null, 9);
        System.out.println(map.size());
        System.out.println(map.get("2"));
        System.out.println(map.get("asdsdfsd"));
        System.out.println(map.get("naoExiste"));
        System.out.println(map.get(null));
    }

    @Test
    public void put() {
        map.put("ola", 0);
        map.put("asdsdfsd", 1);
        map.put("3", 2);
       // map.put("4", 3);
        map.put("2", 7);
        map.put("2", 8);
        System.out.println(map.size());
    }

    @Test
    public void remove() {
        map.put("ola", 0);// hash(Ea) == hash(FB)
        //map.put("asdsdfsd", 1);
        //map.put("3", 2);
        map.put("4", 3);
        map.put("2", 7);
        map.put("2", 8);
        map.put(null, 9);
        //map.put("aa",10);
        //System.out.println(map.size());
        //System.out.println(map.containsKey("4"));
        //System.out.println(map.containsKey("ola"));
        System.out.println(map.containsKey(null));
        map.remove("ola");
        map.remove("2");
        //map.remove("aa");
        System.out.println(map.size());
        System.out.println(map.containsKey("ola"));
        System.out.println(map.containsKey("2"));
        System.out.println(map.containsKey("aa"));
        //System.out.println(map.get(null));
        System.out.println(map.containsKey("4"));
    }

    @Test
    public void putAll() {
    }

    @Test
    public void clear() {
    }

    @Test
    public void keySet() {
    }

    @Test
    public void values() {
    }

    @Test
    public void entrySet() {
    }
}